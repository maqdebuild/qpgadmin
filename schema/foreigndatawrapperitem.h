#pragma once

#include "relationitem.h"

class ForeignDataWrapperItem : public RelationItem
{
    Q_OBJECT
public:
    ForeignDataWrapperItem(TreeItem *_parent, const QSqlRecord &_record);

    virtual ItemType type() const override { return itForeignDataWrapper; }

protected:
    virtual void getSQL() override;
};
