#pragma once

#include "relationitem.h"

class ForeignTableItem : public RelationItem
{
public:
    ForeignTableItem(TreeItem *_parent, const QSqlRecord &_record);

    virtual ItemType type() const override { return itForeignTable; };
};
