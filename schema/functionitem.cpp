#include "functionitem.h"

FunctionItem::FunctionItem(TreeItem *_parent, const QSqlRecord &_record, const QString &_title, const QString &_sql):
    SQLItem(_parent, _record, _title, _S("function"), _sql)
{
}
