#pragma once

#include "sqlitem.h"

class FunctionItem : public SQLItem
{
public:
    FunctionItem(TreeItem *_parent, const QSqlRecord &_record, const QString &_title, const QString &_sql = QString());

    virtual ItemType type() const override { return itFunction; }
};
