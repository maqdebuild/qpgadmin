#include "treeitem.h"
#include "genericsection.h"
#include "functionssection.h"

FunctionsSection::FunctionsSection(TreeItem *_parent):
    GenericSection(_parent, tr("Functions"), _S("functions"))
{
}
