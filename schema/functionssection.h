#pragma once

#include "genericsection.h"
#include "functionitem.h"

class FunctionsSection : public GenericSection
{
public:
    FunctionsSection(TreeItem *_parent);

    FunctionItem* addItem(const QSqlRecord &_record, const QString &_signature, const QString &_sql)
    {
        return make_child<FunctionItem>(_record, _signature, _sql);
    }
};
