#pragma once

// YOU MUST INCLUDE "treeitem.h" and then "genericitem.h"

class GenericSection : public TreeItem
{
public:
    GenericSection(TreeItem *_parent, const QString &_title, const QString &_icon);
    GenericSection(TreeItem *_parent, const QSqlRecord &_record, const QString &_icon);

    virtual ItemType type() const override { return itGenericSection; };
};
