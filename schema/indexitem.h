#pragma once

#include "sqlitem.h"

class IndexItem : public SQLItem
{
public:
    IndexItem(TreeItem *_parent, const QString& _name, const QString &_sql);
    IndexItem(TreeItem *_parent, const std::pair<const QString&, const QString&>& _indexdef);

    virtual ItemType type() const override { return itIndex; }
};
