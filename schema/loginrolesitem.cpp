#include "loginrolesitem.h"

LoginRolesItem::LoginRolesItem(TreeItem *_parent, const QSqlRecord &_record):
    TreeItem(_parent, _record, _S("user"))
{
    setNoChildren();
}

QVariant LoginRolesItem::data(int column, int role, bool) const
{
    return TreeItem::data(column, role, true);
}
