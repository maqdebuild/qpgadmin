#pragma once

#include "treeitem.h"

class LoginRolesItem : public TreeItem
{
public:
    LoginRolesItem(TreeItem *_parent, const QSqlRecord &_record);

    virtual QVariant data(int column, int role, bool) const;

    virtual ItemType type() const override { return itLoginRole; }
};
