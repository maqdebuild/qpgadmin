#include "relationitem.h"
#include "statsmodel.h"
#include "databaseitem.h"

RelationItem::RelationItem(TreeItem *_parent, const QString &_title, const QString &_icon):
    SQLItem(_parent, _title, _icon)
{
}

RelationItem::RelationItem(TreeItem *_parent, const QSqlRecord &_record, const QString &_icon):
    SQLItem(_parent, _record, _icon)
{
}

void RelationItem::loadStats()
{
    auto di = findParent<DatabaseItem>();
    assert(di);
    auto db = di->db;

    auto [success, query] = loadQuery(db, _S("table"), _S("stats"), {{_S(":oid"), oid()}});
    assert(success);

    m_stats = std::make_unique<StatsModel>(query);

    SQLItem::loadStats();
}
