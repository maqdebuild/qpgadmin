#pragma once

#include "sqlitem.h"

class RelationItem : public SQLItem
{
public:
    RelationItem(TreeItem *_parent, const QString &_title, const QString &_icon = _S("table"));
    RelationItem(TreeItem *_parent, const QSqlRecord &_record, const QString &_icon = _S("table"));

protected:
    virtual void loadStats() override;
};
