#include "schemaitem.h"

#include <cassert>
#include <unordered_map>
#include <set>

#include "databaseitem.h"
#include "foreigntableitem.h"
#include "functionitem.h"
#include "functionssection.h"
#include "relationitem.h"
#include "sequenceitem.h"
#include "sequencessection.h"
#include "sqlitem.h"
#include "tableitem.h"
#include "tablessection.h"
#include "triggeritem.h"
#include "triggerssection.h"
#include "typeitem.h"
#include "viewitem.h"
#include "viewssection.h"
#include "indexitem.h"

SchemaItem::SchemaItem(TreeItem *_parent, const QString &_name, const QString &_title, const QString &_icon):
    TreeItem(_parent, _title, _icon),
    m_name(_name)
{
}

SchemaItem::SchemaItem(TreeItem *_parent, const QSqlRecord &_record, const QString &_icon):
    TreeItem(_parent, _record, _icon)
{
    m_name = title();
}

void SchemaItem::load()
{
    if (state() == stUnloaded)
    {
        const auto count = 7;
        beginLoadChildren(7);

        auto di = findParent<DatabaseItem>();
        assert(di);
        auto db = di->db;

        loadRelationObjects(db);
        loadFunctions(db);
        loadTypes(db);
        loadForeignTables(db);
        sort();

        endLoadChildren();
        assert(childCount() == count);
    }
}

void SchemaItem::loadRelationObjects(QSqlDatabase &db)
{
    auto sequences = make_child<SequencesSection>();
    auto tables = make_child<TablesSection>();
    auto views = make_child<ViewsSection>();

    auto [ success, query ] = loadQuery(db, _S("schema"), _S("relations"), {{_S(":name"), name()}});
    assert(success);

    std::unordered_map<QString, std::set<std::pair<QString, QString>>> indicies;

    while(query.next())
    {
        auto kind = query.value(_S("relkind")).toString();
        assert(!kind.isEmpty());
        assert(kind.length() == 1);
        if (kind.isEmpty()) continue;

        switch(kind[0].toLatin1())
        {
            case 'S':
            {
                sequences->addItem(query.record());
                break;
            }
            case 'r':
            {
                tables->addItem(query.record());
                break;
            }
            case 'v':
            case 'm':
            {
                views->addItem(query.record(), kind == _C('m'));
                break;
            }
            case 'i':
            {
                auto tableName = query.value(_S("index_table")).toString();
                auto indexName = query.value(_S("relname")).toString();
                auto indexdef = query.value(_S("indexdef")).toString();

                indicies[tableName].insert({indexName, indexdef});
                break;
            }
            default:
            {
                DEBUG_MSG("Unhandled kind: " << kind << endl);
            }
        }
    }

    const auto count = tables->childCount();
    for(auto i = 0; i < count; i++)
    {
        auto table = tables->child(i);
        auto it = indicies.find(table->title());
        if(it != indicies.end())
        {
            table->make_children<IndexItem>(_S("Indicies"), _S("indexes"), it->second);
        }
    }
}

void SchemaItem::loadFunctions(QSqlDatabase &db)
{
    auto functions = make_child<FunctionsSection>();
    auto triggers = make_child<TriggersSection>();

    auto [success, query] = loadQuery(db, _S("schema"), _S("functions"), {{_S(":name"), name()}});
    assert(success);

    while(query.next())
    {
        auto prorettype = query.value(_S("prorettype"));

        auto rowCount = query.value(_S("lanname")).toInt();
        auto rows = (rowCount == 0 ? _S("") : _S("\n    rows %1").arg(rowCount));

        auto proname = query.value(_S("proname")).toString();
        auto proargnames = pgArrayToList(query.value(_S("proargnames")));
        auto proargtypes = pgArrayToList(query.value(_S("proargtypes")));
        auto proargmodes = pgArrayToList(query.value(_S("proargmodes")));
        auto params = QStringList();

        auto count = proargnames.count();
        if (count)
        {
            assert(proargnames.count() >= proargtypes.count());
            assert(proargmodes.isEmpty() || proargmodes.count() >= proargnames.count());

            auto j = 0;
            for(int i = 0; i < count; i++)
            {
                if (proargmodes.isEmpty() || proargmodes[i] == _S("i"))
                {
                    params.push_back(_S("%1 %2").arg(proargnames[i], proargtypes[j]));
                    j++;
                }
            }
        }
        auto signature = _S("%1(%2)").arg(proname, params.join(_S(", ")));

        auto sql =
        _S("create or replace function %1.%2\n"
           "    returns %3 as\n"
           "$body$\n"
           "%4\n"
           "$body$\n"
           "    language %5 %6\n"
           "    cost %7%8;\n"

        ).arg(name(),
              signature,
              query.value(_S("prorettype")).toString(),
              query.value(_S("prosrc")).toString().trimmed(),
              query.value(_S("lanname")).toString(),
              (query.value(_S("provolatile")).toString() == _S("v") ? _S("volatile") : _S("stable")),
              query.value(_S("procost")).toString(),
              rows
             );

        if (prorettype == _S("trigger"))
        {
            triggers->addItem(query.record(), signature, sql);
        }
        else
        {
            functions->addItem(query.record(), signature, sql);
        }
    }
}

void SchemaItem::loadTypes(QSqlDatabase &db)
{
    make_children<TypeItem>(tr("Types"), _S("types"), loadQuery(db, _S("schema"), _S("types"), {{_S(":name"), name()}}));
}

void SchemaItem::loadForeignTables(QSqlDatabase &db)
{
    make_children<ForeignTableItem>(tr("Foreign Tables"), _S("foreigntables"), db, _S("select foreign_table_name from information_schema.foreign_tables where foreign_table_schema = '%1'order by foreign_table_name").arg(name()));
}

QVariant SchemaItem::data(int column, int role, bool) const
{
    return TreeItem::data(column, role, true);
}
