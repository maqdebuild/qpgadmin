#pragma once

#include "genericsection.h"
#include "sequenceitem.h"

class SequencesSection : public GenericSection
{
public:
    SequencesSection(TreeItem *_parent);

    SequenceItem* addItem(const QSqlRecord &_record)
    {
        return make_child<SequenceItem>(_record);
    }
};
