#include "servergroup.h"

#include "serveritem.h"

#include <QSettings>
#include <QUuid>

ServerGroup::ServerGroup(TreeItem *_parent, const QString &_title):
    TreeItem(_parent, _title, _S("server"))
{
    load();
}

void ServerGroup::load()
{
    if (state() == stUnloaded)
    {
        QSettings settings;

        settings.beginReadArray(_S("groups"));
        settings.setArrayIndex(row());

        auto count = settings.beginReadArray(_S("servers"));
        beginLoadChildren(count);

        for(int i = 0; i < count; i++)
        {
            settings.setArrayIndex(i);
            auto keys = settings.allKeys();

            auto driver = _S("QPSQL");
            if (keys.contains(_S("driver")))
                driver = settings.value(_S("host")).toString();

            auto db = QSqlDatabase::addDatabase(driver, QUuid::createUuid().toString());

            if (keys.contains(_S("host")))
                db.setHostName(settings.value(_S("host")).toString());
            if (keys.contains(_S("port")))
                db.setPort(settings.value(_S("port")).toInt());
            if (keys.contains(_S("user")))
                db.setUserName(settings.value(_S("user")).toString());

            auto options = _S("dbname=%1");
            if (keys.contains(_S("maintenanceDb")))
                db.setConnectOptions(options.arg(settings.value(_S("maintenanceDb")).toString()));
            else
                db.setConnectOptions(options.arg(_S("postgres")));

            QString sslMode;
            if (keys.contains(_S("ssl")))
                sslMode = _S("require");

            if(!sslMode.isEmpty())
                db.setConnectOptions(_S("sslmode=") + sslMode);

            addItem(settings.value(_S("name")).toString(), db);
        }

        settings.endArray(); // servers
        settings.endArray(); // groups

        sort();
        endLoadChildren();
        assert(childCount() == count);
    }
}

ServerItem* ServerGroup::addItem(const QString &name, QSqlDatabase &db)
{
    auto item = make_child<ServerItem>(std::move(db), name);
    if (db.isOpen())
        item->changeIcon(_S("server"));
    return item;
}

ServerItem* ServerGroup::addServer(const QString &name, QSqlDatabase &db)
{
    auto result = addItem(name, db);

    QSettings settings;
    settings.beginWriteArray(_S("groups"));
    settings.setArrayIndex(row());

    settings.setValue(_S("name"), title());

    settings.beginWriteArray(_S("servers"));

    auto count = childCount();
    settings.setArrayIndex(count - 1);
    settings.setValue(_S("name"), name);
    settings.setValue(_S("host"), db.hostName());
    settings.setValue(_S("port"), db.port());
    settings.setValue(_S("user"), db.userName());

    settings.endArray(); // servers
    settings.endArray(); // groups

    sort();

    return result;
}


void ServerGroup::deleteServer(ServerItem *_item)
{
    unload(_item);

    QSettings settings;
    settings.beginWriteArray(_S("groups"));
    settings.setArrayIndex(row());

    settings.setValue(_S("name"), title());

    /**
     * To erase an item from a QSettings array, we must re-write the whole
     * array.  So first, copy all the values _except_ the one to be deleted
     */

    settings.beginWriteArray(_S("servers"));

    typedef std::map<QString, QVariant> ValueMap;
    std::vector<ValueMap> keys;

    auto count = childCount();
    for(int i = 0; i < count; i++)
    {
        settings.setArrayIndex(i);
        if (settings.value(_S("name")) != _item->title())
        {
            ValueMap m;
            for(auto k : settings.allKeys())
                m[k] = settings.value(k);
            keys.push_back(std::move(m));
         }
    }

    settings.endArray(); // servers

    /**
     * Delete the whole array
     */
    settings.remove(_S("servers"));

    /**
     * Rewrite the array without the item.
     */
    settings.beginWriteArray(_S("servers"));

    int i = 0;
    for(auto k : keys)
    {
        settings.setArrayIndex(i);
        for(auto v : k)
        {
            settings.setValue(v.first, v.second);
        }
        i++;
    }

    settings.endArray(); // servers

    endUnloadChildren();
}
