#pragma once

#include "treeitem.h"

#include <QSqlDatabase>

class ServerItem;

class ServerGroup: public TreeItem
{
public:
    ServerGroup(TreeItem *_parent, const QString &_title);

    ServerItem* addServer(const QString &name, QSqlDatabase &db);
    void deleteServer(ServerItem *_item);

    virtual void load();

    virtual ItemType type() const override { return itServerGroup; }
protected:

    ServerItem* addItem(const QString &name, QSqlDatabase &db);
};
