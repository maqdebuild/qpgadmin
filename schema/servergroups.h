#pragma once

#include "treeitem.h"

class ServerGroup;

class ServerGroups: public TreeItem
{
public:
    ServerGroups(TreeItem *_parent);

    ServerGroup* addGroup(const QString &name);

    virtual ItemType type() const override { return itServerGroups; }

protected:
    virtual void load() override;

    int loadOldConfig();

    ServerGroup* addItem(const QString &name);
};
