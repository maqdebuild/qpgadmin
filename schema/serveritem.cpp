#include "serveritem.h"

#include "databaseitem.h"
#include "grouprolesitem.h"
#include "loginrolesitem.h"
#include "tablespaceitem.h"

#include <QMessageBox>
#include <QSqlDriver>
#include <QSqlError>

ServerItem::ServerItem(TreeItem *_parent, QSqlDatabase _db):
    ServerItem(_parent, _db, _db.databaseName())
{
}

ServerItem::ServerItem(TreeItem *_parent, QSqlDatabase _db, const QString &_title):
    TreeItem(_parent, _title, (_db.isOpen() ? _S("server") : _S("serverbad"))),
    db(_db)
{
    setDescription(_S("%1:%2").arg(_db.hostName(), QString::number(_db.port())));
}

void ServerItem::load()
{
    if (state() == stUnloaded)
    {
        QApplication::setOverrideCursor(Qt::WaitCursor);
        QApplication::processEvents(QEventLoop::ExcludeUserInputEvents);
        
        DEBUG_MSG("Server connect: " << db << "\n");
        
        auto result = db.open();
        
        if(!result)
        {
            QApplication::restoreOverrideCursor();
            
            QMessageBox::critical(nullptr, tr("Error"), db.driver()->lastError().databaseText());
            return;
        }

        const auto count = 4;
        beginLoadChildren(count);

        changeIcon(_S("server"));
        make_children<DatabaseItem>(tr("Databases"), _S("databases"), db, _S("select datname from pg_database where datistemplate = false order by 1"));
        make_children<TablespaceItem>(tr("Tablespaces"), _S("tablespaces"), db, _S("select spcname from pg_tablespace order by 1"));
        make_children<GroupRolesItem>(tr("Group Roles"), _S("groups"), db, _S("select rolname from pg_roles where not rolcanlogin order by 1"));
        make_children<LoginRolesItem>(tr("Login Roles"), _S("users"), db, _S("select rolname from pg_roles where rolcanlogin order by 1"));

        endLoadChildren();
        assert(childCount() == count);
        QApplication::restoreOverrideCursor();
    }
}

QVariant ServerItem::data(int column, int role, bool) const
{
    return TreeItem::data(column, role, true);
}
