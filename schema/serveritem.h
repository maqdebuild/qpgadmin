#pragma once

#include "treeitem.h"

class ServerItem: public TreeItem
{
public:
    QSqlDatabase db;

    ServerItem(TreeItem *_parent, QSqlDatabase _db);
    ServerItem(TreeItem *_parent, QSqlDatabase _db, const QString &_title);

    QVariant data(int column, int role, bool) const override;

    virtual ItemType type() const override { return itServer; }

protected:
    virtual void load();
};
