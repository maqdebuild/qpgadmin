select p.oid, proname, lanname, prosrc, proargnames, string_to_array(oidvectortypes(proargtypes), ', ') as "proargtypes",
       proargmodes, t.typname as "prorettype", procost, prorows, prosecdef, provolatile
from pg_proc p
	join pg_namespace ns on p.pronamespace = ns.oid
	join pg_language l on p.prolang = l.oid
	join pg_type t on p.prorettype = t.oid
where nspname = :name
order by 2
