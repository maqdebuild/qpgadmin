SELECT
    seq_scan AS "Sequential scans",
    seq_tup_read AS "Sequential tuples read",
    idx_scan AS "Index scans",
    idx_tup_fetch AS "Index tuples fetched",
    n_tup_ins AS "Tuples inserted",
    n_tup_upd AS "Tuples updated",
    n_tup_del AS "Tuples deleted",
    n_tup_hot_upd AS "Tuples HOT updated",
    n_live_tup AS "Live tuples",
    n_dead_tup AS "Dead tuples",
    heap_blks_read AS "Heap blocks read",
    heap_blks_hit AS "Heap blocks hit",
    idx_blks_read AS "Index blocks read",
    idx_blks_hit AS "Index blocks hit",
    toast_blks_read AS "Toast blocks read",
    toast_blks_hit AS "Toast blocks hit",
    tidx_blks_read AS "Toast index blocks read",
    tidx_blks_hit AS "Toast index blocks hit",
    last_vacuum AS "Last vacuum",
    last_autovacuum AS "Last autovacuum",
    last_analyze AS "Last analyze",
    last_autoanalyze AS "Last autoanalyze",
    pg_relation_size(stat.relid) AS "Table size",
    CASE WHEN cl.reltoastrelid = 0 THEN NULL ELSE pg_relation_size(cl.reltoastrelid)
        + COALESCE((SELECT SUM(pg_relation_size(indexrelid))
                        FROM pg_index WHERE indrelid=cl.reltoastrelid)::int8, 0)
        END AS "Toast table size",
    COALESCE((SELECT SUM(pg_relation_size(indexrelid))
                                FROM pg_index WHERE indrelid=stat.relid)::int8, 0)
        AS "Indexes size"
FROM
    pg_stat_all_tables stat
JOIN
    pg_statio_all_tables statio ON stat.relid = statio.relid
JOIN
    pg_class cl ON cl.oid=stat.relid
WHERE
    stat.relid = :oid
