#pragma once

#include <QAbstractItemModel>
#include <QSqlQuery>
#include <vector>

/**
 * @todo write docs
 */
class StatsModel : public QAbstractTableModel
{
    Q_OBJECT

public:
    StatsModel();
    StatsModel(QSqlQuery _query);

    virtual QVariant headerData(int section, Qt::Orientation orientation, int role) const override;
    virtual QVariant data(const QModelIndex& index, int role) const override;
    virtual int columnCount(const QModelIndex& parent) const override;
    virtual int rowCount(const QModelIndex& parent) const override;

    void addRow(const QString &_field, const QVariant &_value, const QString &_comment = QString());
private:
    struct Row
    {
        QString field;
        QVariant value;
        QString comment;
    };
    std::vector<Row> m_rows;
};
