#pragma once

#include "relationitem.h"

class TableItem : public RelationItem
{
public:
    TableItem(TreeItem *_parent, const QSqlRecord &_record);

    virtual ItemType type() const override { return itTable; }
};
