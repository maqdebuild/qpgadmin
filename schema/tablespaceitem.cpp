#include "tablespaceitem.h"

TablespaceItem::TablespaceItem(TreeItem *_parent, const QString &_title):
    TreeItem(_parent, _title, _S("tablespace"))
{
}

TablespaceItem::TablespaceItem(TreeItem *_parent, const QSqlRecord &_record):
    TreeItem(_parent, _record, _S("tablespace"))
{
}
