#include "treeitem.h"
#include "genericsection.h"
#include "tablessection.h"

TablesSection::TablesSection(TreeItem *_parent):
    GenericSection(_parent, tr("Tables"), _S("tables"))
{
}
