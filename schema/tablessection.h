#pragma once

#include "genericsection.h"
#include "tableitem.h"

class TablesSection : public GenericSection
{
public:
    TablesSection(TreeItem *_parent);

    TableItem* addItem(const QSqlRecord &_record)
    {
        return make_child<TableItem>(_record);
    }
};
