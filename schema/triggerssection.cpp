#include "treeitem.h"
#include "genericsection.h"
#include "triggerssection.h"

TriggersSection::TriggersSection(TreeItem* _parent):
    GenericSection(_parent, tr("Trigger Functions"), _S("triggerfunctions"))
{
}
