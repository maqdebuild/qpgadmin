#include "viewitem.h"

ViewItem::ViewItem(TreeItem *_parent, const QSqlRecord &_record, bool _isMeterialized):
    RelationItem(_parent, _record, _S("view")),
    m_isMeterialized(_isMeterialized)
{
}
