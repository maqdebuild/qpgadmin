#include "treeitem.h"
#include "genericsection.h"
#include "viewssection.h"

ViewsSection::ViewsSection(TreeItem *_parent):
    GenericSection(_parent, tr("Views"), _S("views"))
{
}
