#pragma once

#include "genericsection.h"
#include "viewitem.h"

class ViewsSection : public GenericSection
{
public:
    ViewsSection(TreeItem *_parent);

    ViewItem* addItem(const QSqlRecord &_record, bool _isMeterialized)
    {
        return make_child<ViewItem>(_record, _isMeterialized);
    }
};
