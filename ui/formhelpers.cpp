/*
 * <B1tF0rge> 2014 Todos os direitos reservados
 */

#include "formhelpers.h"
#include "main.h"

#include <QtCore/QSettings>
#include <QtCore/QCryptographicHash>

#include <QtWidgets/QTableView>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QSplitter>

namespace
{

QString getTableViewID(const QTableView *tv)
{
    auto model = tv->model();

    if(model)
    {
        auto colCount = model->columnCount(QModelIndex());
        QCryptographicHash hashParents(QCryptographicHash::Md5);
        QCryptographicHash hashCols(QCryptographicHash::Md5);

        for(int i = 0; i != colCount; i++)
        {
            hashCols.addData(model->headerData(i, Qt::Horizontal, Qt::DisplayRole).toString().toUtf8());
        }

        const QObject *w = tv;

        while(w)
        {
            hashParents.addData(w->objectName().toUtf8());
            w = w->parent();
        }

        return _S("%1_%2_%3")
               .arg(tv->objectName())
               .arg(QString::fromUtf8(hashParents.result().toBase64()))
               .arg(QString::fromUtf8(hashCols.result().toBase64()));
    }

    return QString();
}

}

void setAllTablesViewState(QWidget *parent)
{
    auto allTableViews = parent->findChildren<QTableView *>();

    for(auto tv : allTableViews)
        setTableViewState(tv);
}

void setTableViewState(QTableView *tv)
{
    auto model = tv->model();

    // This can only be done after window is shown and m_model is created
    if(model)
    {
        auto colCount = model->columnCount(QModelIndex());
        auto visibleColount = colCount;

        for(auto i = 0; i != colCount; i++)
        {
            if(tv->isColumnHidden(i))
                visibleColount--;
        }

        QSettings settings;
        settings.beginGroup(_S("grids"));
        settings.beginGroup(getTableViewID(tv));

        if(settings.contains(_S("state")))
        {
            tv->horizontalHeader()->restoreState(settings.value(_S("state")).toByteArray());
        }
        else
        {
            int colWidth = std::max(100.0f, ((float)(tv->width()) * 0.95f) / visibleColount);

            for(int i = 0; i != colCount; i++)
            {
                if(!tv->isColumnHidden(i))
                    tv->setColumnWidth(i, colWidth);
            }
        }
    }
}

void saveAllTablesViewState(const QWidget *parent)
{
    auto allTableViews = parent->findChildren<QTableView *>();

    for(auto tv : allTableViews)
        saveTableViewState(tv);
}

void saveTableViewState(const QTableView *tv)
{
    QAbstractItemModel *model = tv->model();

    if(model)
    {
        QSettings settings;
        settings.beginGroup(_S("grids"));
        settings.beginGroup(getTableViewID(tv));
        settings.setValue(_S("state"), tv->horizontalHeader()->saveState());
    }
}

static QString getWidgetID(const QWidget *widget)
{
    QCryptographicHash hashParents(QCryptographicHash::Md5);

    const QObject *w = widget;

    while(w)
    {
        hashParents.addData(w->objectName().toUtf8());
        w = w->parent();
    }

    auto result = QString::fromUtf8(hashParents.result().toBase64());
    result.replace(_C('\\'), _C('_'));
    result.replace(_C('/'), _C('_'));
    return result;
}

void setAllSplitersState(QWidget *parent)
{
    auto allSplitters = parent->findChildren<QSplitter *>();

    for(auto s : allSplitters)
        setSpliterState(s);
}

void setSpliterState(QSplitter *splitter)
{
    QSettings settings;
    settings.beginGroup(_S("splitters"));
    settings.beginGroup(getWidgetID(splitter));

    if(settings.contains(_S("state")))
        splitter->restoreState(settings.value(_S("state")).toByteArray());
}

void saveAllSplitersState(QWidget *parent)
{
    auto allSplitters = parent->findChildren<QSplitter *>();

    for(auto s : allSplitters)
        saveSpliterState(s);
}

void saveSpliterState(QSplitter *splitter)
{
    QSettings settings;
    settings.beginGroup(_S("splitters"));
    settings.beginGroup(getWidgetID(splitter));
    settings.setValue(_S("state"), splitter->saveState());
}

void enableActionsInCurrentWindow(QWidget *window, std::initializer_list<QAction *> _acActions)
{
    for(auto ac : _acActions)
    {
        window->addAction(ac);
        ac->setShortcutContext(Qt::WindowShortcut);
    }
}

void saveWidgetState(const QWidget *widget)
{
    QSettings settings;
    settings.beginGroup(_S("widget"));
    settings.beginGroup(getWidgetID(widget));
    settings.setValue(_S("windowState"), (int)widget->windowState());
    settings.setValue(_S("geometry"), widget->geometry());
}

void setWidgetState(QWidget *widget)
{
    QSettings settings;
    settings.beginGroup(_S("widget"));
    auto id = getWidgetID(widget);
    DEBUG_MSG(settings.childGroups() << endl);
    if (settings.childGroups().contains(id))
    {
        settings.beginGroup(id);
        QFlags<Qt::WindowState> ws;
        ws &= settings.value(_S("windowState")).toInt();
        widget->setWindowState(ws);
        widget->setGeometry(settings.value(_S("geometry")).toRect());
    }
}
