#include "qssh2tunnel.h"

#include <atomic>

std::atomic_int s_userCounter;

QSsh2Tunnel::QSsh2Tunnel(QObject *_parent):
    QObject(_parent)
{
    auto cntr = std::atomic_fetch_add(&s_userCounter, 1);
    if (cntr == 1) // The value before the sub operation
    {
        auto ret = libssh2_init(0);
        if(ret != 0)
        {
            qDebug("libssh2 initialization failed (%d)", ret);
        }
    }

    connect(this, &QSsh2Tunnel::started, this, &QSsh2Tunnel::onStarted);
}

QSsh2Tunnel::~QSsh2Tunnel()
{
    closeSocket();

    auto cntr = std::atomic_fetch_sub(&s_userCounter, 1);
    if (cntr == 1) // The value before the sub operation
    {
        libssh2_exit();
    }
}

void QSsh2Tunnel::closeSocket()
{
    if(m_listener)
    {
        libssh2_channel_forward_cancel(m_listener);
        m_listener = nullptr;
    }

    if (m_ssh_session)
    {
        libssh2_session_disconnect(m_ssh_session, "Client disconnecting normally");
        libssh2_session_free(m_ssh_session);
        m_ssh_session = nullptr;
    }

    auto close_sock = [](auto &sock)
    {
        delete sock;
        sock = nullptr;
    };

    close_sock(m_localSocket);
    close_sock(m_hostSocket);

    m_localPort = 0;

    m_user.clear();
    m_hostAddr.clear();
    m_hostPort = 0;

    m_peerAddr.clear();
    m_peerPort = 0;
}

void QSsh2Tunnel::makeSocket()
{
    closeSocket();

    m_localSocket = new QTcpServer(this);
    connect(m_localSocket, &QTcpServer::acceptError, this, &QSsh2Tunnel::socketError);
    connect(m_localSocket, &QTcpServer::newConnection, this, &QSsh2Tunnel::newConnection);

    m_hostSocket = new QTcpSocket(this);
    connect(m_hostSocket, QOverload<QAbstractSocket::SocketError>::of(&QAbstractSocket::error),
            this, &QSsh2Tunnel::socketError);
    connect(m_hostSocket, &QTcpSocket::connected, this, &QSsh2Tunnel::hostConnected);
    connect(m_hostSocket, &QTcpSocket::connected, this, &QSsh2Tunnel::connected);
}

void QSsh2Tunnel::onStarted()
{
    m_localSocket->listen();

    /* Must use non-blocking IO hereafter due to the current libssh2 API */
    libssh2_session_set_blocking(m_ssh_session, 0);
}

void QSsh2Tunnel::connectTunnel(int localPort,

                                QString user,
                                QString hostAddr,
                                int hostPort,

                                QString peerAddr,
                                int peerPort)
{
    makeSocket();
    m_localPort = localPort;

    m_user = user;
    m_hostAddr = hostAddr;
    m_hostPort = hostPort;

    m_peerAddr = peerAddr;
    m_peerPort = peerPort;

    m_hostSocket->connectToHost(hostAddr, hostPort);
}

void QSsh2Tunnel::emitLastSshError()
{
    auto n = libssh2_session_last_errno(m_ssh_session);

    char *msg = nullptr;
    int len = 0;

    auto r = libssh2_session_last_error(m_ssh_session, &msg, &len, 0);

    if (r == 0 || n)
    {
        auto errorMessage = QString::fromLocal8Bit(msg, len);

        qDebug("SSH Error: %d - %s", n, errorMessage.toLocal8Bit().data());
        emit sshError(n, errorMessage);
    }
}

void QSsh2Tunnel::hostConnected()
{
    /* Create a session instance and start it up. This will trade welcome
     * banners, exchange keys, and setup crypto, compression, and MAC layers
     */
    m_ssh_session = libssh2_session_init();

#ifndef NDEBUG
    libssh2_trace(m_ssh_session, LIBSSH2_TRACE_SOCKET | LIBSSH2_TRACE_AUTH | LIBSSH2_TRACE_CONN | LIBSSH2_TRACE_ERROR | LIBSSH2_TRACE_PUBLICKEY);
#endif

    if(libssh2_session_handshake(m_ssh_session, m_hostSocket->socketDescriptor()))
    {
        emitLastSshError();
        emit sshSocketError(FailedToHandshake);
        m_hostSocket->close();
        return;
    }

    /* At this point we havn't authenticated. The first thing to do is check
     * the hostkey's fingerprint against our known hosts Your app may have it
     * hard coded, may go to a file, may present it to the user, that's your
     * call
     */
    auto fingerprint = libssh2_hostkey_hash(m_ssh_session, LIBSSH2_HOSTKEY_HASH_SHA1);

#define FNGP(IDX) (unsigned char)fingerprint[IDX]
    qDebug("Fingerprint: %02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",
           FNGP(0), FNGP(1), FNGP(2), FNGP(3), FNGP(4), FNGP(5), FNGP(6), FNGP(7), FNGP(8), FNGP(9),
           FNGP(10), FNGP(11), FNGP(12), FNGP(13), FNGP(14), FNGP(15), FNGP(16), FNGP(17), FNGP(18), FNGP(19)
          );

    /* check what authentication methods are available */
    auto u = m_user.toLocal8Bit();
    auto userauthlist = libssh2_userauth_list(m_ssh_session, u.data(), u.size());

    qDebug("Authentication methods: %s", userauthlist);
    AuthenticationMethodsSet authMethods;
    if(strstr(userauthlist, "password") != NULL) {
        authMethods.insert(Password);
    }
    if(strstr(userauthlist, "keyboard-interactive") != NULL) {
        authMethods.insert(KeyboardInteractive);
    }
    if(strstr(userauthlist, "publickey") != NULL) {
        authMethods.insert(Publickey);
    }
    emit authenticate(authMethods);
}

void auth_callback(LIBSSH2_SESSION *session, char **newpw, int *newpw_len,  void **abstract)
{
    qDebug("auth_callback");
}

bool QSsh2Tunnel::sendAuthPassword(QString password, std::optional<QString> user)
{
    auto u = (user ? user.value() : m_user).toLocal8Bit();
    auto p = password.toLocal8Bit();

    auto ret = libssh2_userauth_password_ex(m_ssh_session,
                                            u.data(), u.size(),
                                            p.data(), p.size(),
                                            auth_callback
                                           );

    qDebug("libssh2_userauth_password: %d", ret);

    if(ret == 0)
        emit started();
    else
        emitLastSshError();

    return ret == 0;
}

bool QSsh2Tunnel::sendAuthFile(QString privFile, QString pubFile, QOptionalString password, QOptionalString user)
{
    auto u = (user ? user.value() : m_user).toLocal8Bit();
    auto p = (password ? password.value().toLocal8Bit() : QByteArray());

    int ret;

    do
    {
        ret = libssh2_userauth_publickey_fromfile_ex(m_ssh_session,
                                                      u.data(), u.size(),
                                                      pubFile.toLocal8Bit().data(),
                                                      privFile.toLocal8Bit().data(),
                                                      p.data());
    } while(ret == LIBSSH2_ERROR_EAGAIN);

    qDebug("libssh2_userauth_password: %d", ret);

    if(ret == 0)
        emit started();
    else
        emitLastSshError();

    return ret == 0;
}

void QSsh2Tunnel::newConnection()
{
    while(m_localSocket->hasPendingConnections())
    {
        Channels chan;

        chan.socket = m_localSocket->nextPendingConnection();

        auto shost = chan.socket->peerAddress();
        auto sport = chan.socket->peerPort();

        qDebug("Forwarding connection from %s:%d here to remote %s:%d",
               shost.toString().toLocal8Bit().data(), sport,
               m_peerAddr.toLocal8Bit().data(), m_peerPort);

        chan.channel = libssh2_channel_direct_tcpip_ex(m_ssh_session,
                                                       m_peerAddr.toLocal8Bit().data(), m_peerPort,
                                                       shost.toString().toLocal8Bit().data(), sport);

        if(!chan.channel)
        {
            qDebug("Could not open the direct-tcpip channel! "
                    "(Note that this can be a problem at the server!"
                    " Please review the server logs.)");
            emit sshSocketError(FailedToOpenChannel);
            emitLastSshError();
            continue;
        }

        connect(chan.socket, &QIODevice::readyRead, [&chan] { chan.read(); });

        m_openChannels.push_back(std::move(chan));
    }
}

QSsh2Tunnel::Channels::Channels(QSsh2Tunnel::Channels && other)
{
    channel = other.channel;
    socket = other.socket;

    other.channel = nullptr;
    other.socket = nullptr;
}

QSsh2Tunnel::Channels::~Channels()
{
    delete socket;
    libssh2_channel_free(channel);
}

void QSsh2Tunnel::Channels::read()
{
    size_t bufferSize = 1400;
    char buffer[bufferSize];

    auto len = socket->bytesAvailable();
    typeof(len) totalWritten = 0;
    while(totalWritten < len)
    {
        auto r = socket->read(buffer, bufferSize);
        typeof(r) wr = 0;

        while(wr < r)
        {
            auto i = libssh2_channel_write(channel, buffer + wr, r - wr);

            if(LIBSSH2_ERROR_EAGAIN == i)
            {
                continue;
            }

            if(i < 0)
            {
                qDebug("libssh2_channel_write: %ld", i);
                delete socket;
                socket = nullptr;
                return;
            }

            wr += i;
            totalWritten += i;
        }
    }
}

void QSsh2Tunnel::Channels::write()
{
}
