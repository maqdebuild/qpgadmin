#pragma once

#include <QtCore/QString>
#include <QtNetwork/QTcpServer>
#include <QtNetwork/QTcpSocket>

#include <libssh2.h>
#include <unordered_set>
#include <optional>
#include <memory>
#include <vector>

/**
 * @todo write docs
 */
class QSsh2Tunnel : public QObject
{
    Q_OBJECT

public:
    typedef std::optional<QString> QOptionalString;

    QSsh2Tunnel(QObject *_parent = nullptr);
    virtual ~QSsh2Tunnel();

    auto localSocket()
    {
        return m_localSocket;
    }
    auto hostSocket()
    {
        return m_hostSocket;
    }

    void connectTunnel(int localPort,

                       QString user,
                       QString hostAddr,
                       int hostPort,

                       QString peerAddr,
                       int peerPort);


    bool sendAuthPassword(QString password, QOptionalString user = QOptionalString());
    bool sendAuthFile(QString privFile, QString pubFile, QOptionalString password = QOptionalString(), QOptionalString user = QOptionalString());

    enum AuthenticationMethods
    {
        Password,
        KeyboardInteractive,
        Publickey
    };
    typedef std::unordered_set<AuthenticationMethods> AuthenticationMethodsSet;

    enum SshSocketError
    {
        FailedToHandshake,
        FailedToOpenChannel,
    };

private:
    QTcpSocket *m_hostSocket = nullptr;
    QTcpServer *m_localSocket = nullptr;
    LIBSSH2_SESSION *m_ssh_session = nullptr;
    LIBSSH2_LISTENER *m_listener = nullptr;

    struct Channels
    {
        Channels(Channels &) = delete;
        Channels(const Channels &) = delete;

        Channels() {}
        Channels(Channels &&other);
        ~Channels();

        void read();
        void write();

        QTcpSocket *socket = nullptr;
        LIBSSH2_CHANNEL *channel = nullptr;
    };
    std::vector<Channels> m_openChannels;

    int m_localPort = 0;

    QString m_user;
    QString m_hostAddr;
    int m_hostPort = 0;

    QString m_peerAddr;
    int m_peerPort = 0;

    void closeSocket();
    void makeSocket();

protected:
    void emitLastSshError();

protected slots:
    virtual void hostConnected();
    virtual void newConnection();
    virtual void onStarted();

signals:
    void socketError(QAbstractSocket::SocketError socketError);
    void sshSocketError(SshSocketError sshSocketError);
    void sshError(int code, QString message);

    void authenticate(AuthenticationMethodsSet supportedAuthenticationMethods);

    void connected();
    void started();
};
