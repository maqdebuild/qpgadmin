#include "server.h"
#include "ui_server.h"
#include "main.h"

#include <QSettings>

Server::Server(QWidget *parent):
    QDialog(parent)
{
    m_ui = new Ui::Server();
    m_ui->setupUi(this);

    auto cbGroup = m_ui->cbGroup;

    QSettings settings;

    auto count = settings.beginReadArray(_S("groups"));

    for(int i = 0; i < count; i++)
    {
        settings.setArrayIndex(i);
        cbGroup->addItem(settings.value(_S("name")).toString());
    }

    settings.endArray();

    auto cbColour = m_ui->cbColour;
    cbColour->addItems(QColor::colorNames());
}

void Server::setCurrentGroup(const QString &group)
{
    m_ui->cbGroup->setCurrentText(group);
}

QString Server::name() const
{
    return m_ui->leName->text();
}

QString Server::host() const
{
    return m_ui->leHost->text();
}

ushort Server::port() const
{
    return m_ui->lePort->text().toUShort();
}

QString Server::service() const
{
    return m_ui->leService->text();
}

QString Server::maintenanceDb() const
{
    return m_ui->cbMaintenanceDB->currentText();
}

QString Server::username() const
{
    return m_ui->leUsername->text();
}

QString Server::password() const
{
    return m_ui->lePassword->text();
}

bool Server::storePassword() const
{
    return m_ui->cbStorePassword->isChecked();
}

QColor Server::colour() const
{
    return QColor(m_ui->cbColour->currentText());
}

QString Server::group() const
{
    return m_ui->cbGroup->currentText();
}
