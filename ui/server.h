#pragma once

#include <QDialog>
#include <QColor>

namespace Ui
{
class Server;
}

class Server : public QDialog
{
    Q_OBJECT
private:
    Ui::Server *m_ui = nullptr;

public:
    Server(QWidget *parent);

    void setCurrentGroup(const QString &group);

    QString name() const;
    QString host() const;
    ushort port() const;
    QString service() const;
    QString maintenanceDb() const;
    QString username() const;
    QString password() const;
    bool storePassword() const;
    QColor colour() const;
    QString group() const;
};
