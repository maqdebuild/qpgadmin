#include "main.h"
#include "sqlsyntaxhighlighter.h"

#include <QTextDocument>

SQLSyntaxHighlighter::SQLSyntaxHighlighter(QTextDocument *parent)
    : QSyntaxHighlighter(parent)
{
    document()->setDefaultFont(_S("Monospace"));

    HighlightingRule rule;

    keywordFormat.setForeground(Qt::darkBlue);
    QStringList keywordPatterns;

//#define KW(str) _S("\\b # str # \\b")
#define KW(str) _S("\\b" # str "\\b")

    keywordPatterns
        << KW(ADD)
        << KW(ALL)
        << KW(ALTER)
        << KW(AND)
        << KW(ANY)
        << KW(ARRAY)
        << KW(AS)
        << KW(BEGIN)
        << KW(BY)
        << KW(CASCADE)
        << KW(CASE)
        << KW(CONFLICT)
        << KW(CONSTRAINT)
        << KW(COST)
        << KW(CREATE)
        << KW(DECLARE)
        << KW(DEFAULT)
        << KW(DELETE)
        << KW(DISTINCT)
        << KW(DO)
        << KW(EACH)
        << KW(ELSE)
        << KW(END)
        << KW(EXCEPTION)
        << KW(EXECUTE)
        << KW(FOR)
        << KW(FOREACH)
        << KW(FOREIGN)
        << KW(FOUND)
        << KW(FROM)
        << KW(FUNCTION)
        << KW(GRANT)
        << KW(GROUP)
        << KW(IF)
        << KW(IN)
        << KW(INDEX)
        << KW(INNER)
        << KW(INSERT)
        << KW(INTO)
        << KW(JOIN)
        << KW(KEY)
        << KW(LANGUAGE)
        << KW(LEFT)
        << KW(LIMIT)
        << KW(LOOP)
        << KW(MATERIALIZED)
        << KW(NAME)
        << KW(NOT)
        << KW(NOTHING)
        << KW(NULL)
        << KW(ON)
        << KW(ONLY)
        << KW(OR)
        << KW(ORDER)
        << KW(OUTER)
        << KW(OWNER)
        << KW(PERFORM)
        << KW(PRIMARY)
        << KW(PROCEDURE)
        << KW(RAISE)
        << KW(RECURSIVE)
        << KW(REFERENCES)
        << KW(REPLACE)
        << KW(RESTRICT)
        << KW(RETURN)
        << KW(RETURNING)
        << KW(RETURNS)
        << KW(RIGHT)
        << KW(ROW)
        << KW(ROWS)
        << KW(SELECT)
        << KW(SET)
        << KW(STABLE)
        << KW(TABLE)
        << KW(THEN)
        << KW(TO)
        << KW(UNION)
        << KW(UNIQUE)
        << KW(UPDATE)
        << KW(USING)
        << KW(VALUES)
        << KW(VIEW)
        << KW(VOID)
        << KW(VALOTILE)
        << KW(WHEN)
        << KW(WHERE)
        << KW(WITH)
        ;

    foreach (const QString &pattern, keywordPatterns) {
        rule.pattern = QRegularExpression(pattern, QRegularExpression::CaseInsensitiveOption);
        rule.format = keywordFormat;
        highlightingRules.append(rule);
    }

    //classFormat.setFontWeight(QFont::Bold);
//     classFormat.setForeground(Qt::darkMagenta);
//     rule.pattern = QRegularExpression(_S("\\bQ[A-Za-z]+\\b"));
//     rule.format = classFormat;
//     highlightingRules.append(rule);

    singleLineCommentFormat.setForeground(Qt::darkRed);
    rule.pattern =QRegularExpression(_S("\'.*\'"));
    rule.format = singleLineCommentFormat;
    highlightingRules.append(rule);

    quotationFormat.setForeground(Qt::darkMagenta);
    rule.pattern = QRegularExpression(_S("\".*\""));
    rule.format = quotationFormat;
    highlightingRules.append(rule);

    //functionFormat.setFontItalic(true);
//     functionFormat.setForeground(Qt::darkGray);
//     rule.pattern = QRegularExpression(_S("\\b[A-Za-z0-9_]+(?=\\()"));
//     rule.format = functionFormat;
//     highlightingRules.append(rule);

    //commentStartExpression = QRegularExpression(_S("--"));
    //commentEndExpression = QRegularExpression(_S("$"));
    commentFormat.setFontItalic(true);
    commentFormat.setForeground(Qt::darkGreen);
    rule.pattern = QRegularExpression(_S("(\\s|^)(--.*$)"));
    rule.format = commentFormat;
    highlightingRules.append(rule);

    castFormat.setForeground(Qt::darkYellow);
    rule.pattern = QRegularExpression(_S("::[A-Za-z0-9_]+"));
    rule.format = castFormat;
    highlightingRules.append(rule);

    numberFormat.setForeground(Qt::darkGreen);
    rule.pattern = QRegularExpression(_S("\\b[0-9_]+\\b"));
    rule.format = numberFormat;
    highlightingRules.append(rule);
}

void SQLSyntaxHighlighter::highlightBlock(const QString &text)
{
    foreach (const HighlightingRule &rule, highlightingRules) {
        QRegularExpressionMatchIterator matchIterator = rule.pattern.globalMatch(text);
        while (matchIterator.hasNext()) {
            QRegularExpressionMatch match = matchIterator.next();
            setFormat(match.capturedStart(), match.capturedLength(), rule.format);
        }
    }
    setCurrentBlockState(0);

//     int startIndex = 0;
//     if (previousBlockState() != 1)
//         startIndex = text.indexOf(commentStartExpression);
//
//     while (startIndex >= 0) {
//         QRegularExpressionMatch match = commentEndExpression.match(text, startIndex);
//         int endIndex = match.capturedStart();
//         int commentLength = 0;
//         if (endIndex == -1) {
//             setCurrentBlockState(1);
//             commentLength = text.length() - startIndex;
//         } else {
//             commentLength = endIndex - startIndex
//                             + match.capturedLength();
//         }
//         setFormat(startIndex, commentLength, multiLineCommentFormat);
//         startIndex = text.indexOf(commentStartExpression, startIndex + commentLength);
//     }
}
