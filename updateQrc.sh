#!/bin/bash

BASE=$(realpath $(dirname $0))

cd $BASE

FILE=$BASE/res.qrc

addfile()
{
    if [ -z "$2" ]; then
        a=$(basename $1)
    else
        a=$2
    fi
    echo -e "\t\t<file alias=\"$a\">$1</file>" >> $FILE
}

echo -e "<RCC>\n\t<qresource prefix=\"ui\">" > $FILE

for f in $(find ui -maxdepth 1 -type f -name "*.qml" | sort ); do
    addfile $f
done

echo -e "\t</qresource>\n\t<qresource prefix=\"images\">" >> $FILE

for f in $(find ui/images -maxdepth 1 -type f -name "*.png" -or -name "*.ico" | sort); do
    addfile $f
done

echo -e "\t</qresource>\n\t<qresource prefix=\"schema\">" >> $FILE

for f in $(find schema/images -maxdepth 1 -type f -name "*.png" -or -name "*.ico" | sort); do
    addfile $f
done

echo -e "\t</qresource>\n\t<qresource prefix=\"sqls\">" >> $FILE

for f in $(find . -type f -name "*.sql" | sort ); do
    addfile $f ${f:14}
done

echo -e "\t</qresource>\n</RCC>" >> $FILE
